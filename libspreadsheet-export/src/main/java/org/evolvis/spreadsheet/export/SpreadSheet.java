/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package org.evolvis.spreadsheet.export;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Represents a Spreadsheet-Document, and accordingly provides methods for
 * creating tables, table-rows and table-cells.
 * 
 * API should be used as the following tree-structure:
 * 
 * <pre>
 * {@link SpreadSheetFactory}.getSpreadSheet(SpreadSheetHelper.TYPE_*)
 * init()
 * 
 * openTable("Tabelle 1", 3)
 *   openRow()
 *     addCell("A1")
 *     addCell("B1")
 *     addCell("C1")
 *   closeRow()
 *   openRow()
 *     addCell("A2")
 *     addCell("")
 *     addCell(null)
 *   closeRow()
 * closeTable()
 * 
 * save(OutputStream)
 * </pre>
 * 
 * @author Christoph Jerolimov
 */
public interface SpreadSheet {
	/**
	 * Sets a property for this document.
	 * 
	 * Should be used before init().
	 * 
	 * @param key the key to set
	 * @param value value for that key
	 */
	public void setProperty(String key, String value) throws IOException;

	/**
	 * Returns a property of this document
	 * 
	 * @param key key to get
	 * @return the value of the key
	 */
	public String getProperty(String key) throws IOException;

	/**
	 * Initialises the SpreadSheetDocument.
	 * 
	 * @throws IOException
	 */
	public void init() throws IOException;

	/**
	 * Saves the document to the given OutputStream.
	 * 
	 * <strong>Does NOT close the stream!</strong>
	 * 
	 * @param outputStream
	 */
	public void save(OutputStream outputStream) throws IOException;

	/**
	 * Returns the mime-type of this document.
	 * 
	 * @return MimeType
	 */
	public String getContentType();

	/**
	 * Returns the standard file-extension of this document.
	 * 
	 * @return file-extension
	 */
	public String getFileExtension();

	/**
	 * Opens a new table with the given name.
	 * 
	 * @param name name of the table
	 * @param colCount Specifies the amount of columns for the table.
	 */
	public void openTable(String name, int colCount);

	/**
	 * Closes the table.
	 */
	public void closeTable();

	/**
	 * Opens a new row.
	 */
	public void openRow();

	/**
	 * Closes the row.
	 */
	public void closeRow();

	/**
	 * Adds a cell with the given content.
	 * 
	 * @param content content of cell
	 */
	public void addCell(Object content);
}

/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package org.evolvis.spreadsheet.export;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Example Application
 * 
 * @author Christoph Jerolimov
 */
public class Sample {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			testXML(SpreadSheetFactory.getSpreadSheet(SpreadSheetFactory.TYPE_SXC_CONTENT));
			testXML(SpreadSheetFactory.getSpreadSheet(SpreadSheetFactory.TYPE_ODS_CONTENT));
			testFile(SpreadSheetFactory.getSpreadSheet(SpreadSheetFactory.TYPE_SXC_DOCUMENT));
			testFile(SpreadSheetFactory.getSpreadSheet(SpreadSheetFactory.TYPE_ODS_DOCUMENT));
			testFile(SpreadSheetFactory.getSpreadSheet(SpreadSheetFactory.TYPE_XLS_DOCUMENT));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void testXML(SpreadSheet spreadSheet) throws IOException {
		spreadSheet.init();
		test(spreadSheet);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		spreadSheet.save(os);
		os.close();
//		System.out.println(os.toString());
	}

	public static void testFile(SpreadSheet spreadSheet) throws IOException {
		spreadSheet.init();
		test(spreadSheet);
		FileOutputStream os = null;
		
		try {
			
			os = new FileOutputStream(getFilename(spreadSheet.getFileExtension()));
			spreadSheet.save(os);
			
		} finally {
			
			if(os != null)
				os.close();
		}
	}

	public static void test(SpreadSheet content) throws IOException {
		
		// Create Random Generator
		Random random = new Random();
		
		content.openTable("Tabelle 1", 3);
		content.openRow();
		content.addCell("A1");
		content.addCell("B1");
		content.addCell("C1");
		content.addCell("D1");
		content.closeRow();
		content.openRow();
		content.addCell(null);
		content.addCell("A2");
		content.addCell("");
		content.closeRow();
		content.openRow();
		content.addCell(Integer.valueOf(random.nextInt()));
		content.addCell(Long.valueOf(random.nextLong()));
		content.addCell(Double.valueOf(random.nextDouble()));
		content.closeRow();
		content.openRow();
		content.addCell(BigInteger.valueOf(random.nextLong()));
		content.addCell(BigDecimal.valueOf(random.nextLong()));
		content.addCell(new Date());
		content.closeRow();
		content.openRow();
		content.addCell("A3");
		content.addCell("<test>");
		content.addCell("C3\n\ntest");
		content.closeRow();
		content.closeTable();
	}

	/**
	 * @return File on Desktop containing date and time in filename.
	 * @param extension file-extension
	 */
	protected static File getFilename(String extension) {
		// Should work with Windows + KDE + Gnome.
		return new File(
				System.getProperty("user.home") + File.separator + "Desktop" + File.separator +
				new SimpleDateFormat("MM-dd_HH-mm").format(new Date()) +
				'.' + extension);
	}
}

/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package org.evolvis.spreadsheet.export;

import java.util.ResourceBundle;

import org.evolvis.spreadsheet.export.csv.CSVDocument;
import org.evolvis.spreadsheet.export.ods.ODSContent;
import org.evolvis.spreadsheet.export.ods.ODSDocument;
import org.evolvis.spreadsheet.export.sxc.SXCContent;
import org.evolvis.spreadsheet.export.sxc.SXCDocument;
import org.evolvis.spreadsheet.export.xls.XLSSpreadSheet;


/**
 * Helper-class for SpreadSheets.
 * 
 * @author Christoph Jerolimov
 */
public class SpreadSheetFactory {
	/** ResourceBundle */
	public static final ResourceBundle bundle = ResourceBundle.getBundle("org.evolvis.spreadsheet.export.messages");

	/** Specifier for a SpreadSheet-Instance, which is capable of creating CSV-files. */
	public static final String TYPE_CSV_DOCUMENT = "csv";
	
	/** Specifier for a SpreadSheet-Instance, which is capable of creating SXC-documents (Zip-Archive). */
	public static final String TYPE_SXC_DOCUMENT = "sxc-document";
	
	/** Specifier for a SpreadSheet-Instance, which is capable of creating SXC-files (No Zip-Archive, just the content.xml). */
	public static final String TYPE_SXC_CONTENT = "sxc-content";
	
	/** Specifier for a SpreadSheet-Instance, which is capable of creating OpenDocument-files (Zip-Archive). */
	public static final String TYPE_ODS_DOCUMENT = "ods-document";
	
	/** Specifier for a SpreadSheet-Instance, which is capable of creating OpenDocument-files (No Zip-Archive, just the content.xml). */
	public static final String TYPE_ODS_CONTENT = "ods-content";
	
	/** Specifier for a SpreadSheet-Instance, which is capable of creating Excel-files. */
	public static final String TYPE_XLS_DOCUMENT = "xls";
	
	/** Specifier for the default SpreadSheet-Instance, which is currently {@link #TYPE_ODS_DOCUMENT}. */
	public static final String TYPE_DEFAULT = TYPE_ODS_DOCUMENT;

	/**
	 * Returns a SpreadSheet-Instance according to the given type-specifier.
	 * 
	 * Default is {@link #TYPE_ODS_DOCUMENT}
	 * 
	 * @param type see type-constants TYPE_*
	 * @return SpreadSheetContent
	 */
	public static SpreadSheet getSpreadSheet(String type) {
		if (TYPE_CSV_DOCUMENT.equals(type))
			return new CSVDocument();
		
		else if (TYPE_SXC_CONTENT.equals(type))
			return new SXCContent();
		
		else if (TYPE_SXC_DOCUMENT.equals(type))
			return new SXCDocument();
		
		else if (TYPE_ODS_CONTENT.equals(type))
			return new ODSContent();
		
		else if (TYPE_ODS_DOCUMENT.equals(type))
			return new ODSDocument();
		
		else if (TYPE_XLS_DOCUMENT.equals(type))
			return new XLSSpreadSheet();
		
		else
			return getSpreadSheet(TYPE_DEFAULT);
	}
}

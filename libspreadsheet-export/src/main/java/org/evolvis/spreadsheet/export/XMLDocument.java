/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package org.evolvis.spreadsheet.export;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * 
 * Provides methods for loading and saving XML-documents
 * 
 * @author Christoph Jerolimov
 */
public class XMLDocument {
	protected Document document;

	/**
	 * Loads a XML-document from the given InputStream
	 * 
	 * @param inputStream
	 * @throws IOException
	 */
	public void loadDocument(InputStream inputStream) throws IOException {
		try {
			document = getDocument();
			Source source = new StreamSource(new InputStreamReader(inputStream, "UTF-8"));
			source.setSystemId("file:/tarent");
			Result result = new DOMResult(document);
			getTransformer().transform(source, result);
		} catch (Exception e) {
			throwIOException(e);
		}
	}

	/**
	 * 
	 * Writes a XML-document to the given OutputStream
	 * 
	 * @param outputStream
	 * @throws IOException
	 */
	public void saveDocument(OutputStream outputStream) throws IOException {
		try {
			Source source = new DOMSource(document);
			Result result = new StreamResult(new OutputStreamWriter(outputStream, "UTF-8"));
			getTransformer().transform(source, result);
		} catch (Exception e) {
			throwIOException(e);
		}
	}

	/**
	 * 
	 * Returns the first node-element from this document
	 * which has the same name as the given name
	 * 
	 * @param name Name of the node to return
	 * @return Node
	 */
	protected Node getNode(String name) {
		return document.getElementsByTagName(name).item(0);
	}

	protected Transformer getTransformer() throws TransformerConfigurationException, TransformerFactoryConfigurationError {
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
		return transformer;
	}

	protected Document getDocument() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		return factory.newDocumentBuilder().newDocument();
	}

	/**
	 * This method takes an arbitary Exception-object and throws it as an IOException (if applicable)
	 * or wraps it into a new IOException object which then will be thrown.
	 * 
	 * Error-message and Stacktrace will be conserved.
	 * 
	 * @param t
	 * @throws IOException ALWAYS!
	 */
	protected static void throwIOException(Throwable t) throws IOException {
		if (t instanceof IOException)
			throw (IOException)t;
		else if (t instanceof RuntimeException)
			throw (RuntimeException)t;
		else if (t instanceof Error)
			throw (Error)t;
		
		IOException e = new IOException(t.toString());
		e.setStackTrace(t.getStackTrace());
		throw e;
	}
}
